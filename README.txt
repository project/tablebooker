
-- SUMMARY --

Integrates tablebooker.be with your website. The module provides a block with a 
reservation form. This form can be placed on a website to accept reservations 
for a restaurant on tablebooker.be.

For more information, go to http://www.tablebooker.be

-- REQUIREMENTS --

This module is intended for restaurant websites. In order to use the module the 
restaurant needs to be registered with tablebooker.be and have an active 
tablemanager.be account.

-- INSTALLATION --

* Place the entirety of this directory in sites/all/modules/tablebooker

* Navigate to administration >> modules. Enable tablebooker.

-- CONFIGURATION --

* Navigate to administration >> configuration >> system >> tablebooker
Enter the restaurant id that can be found on 
https://www.tablemanager.be/pages/embed

* Go to structure >> blocks and place the tablebooker reservation block 
on the desired page and region.

-- CONTACT --

current maintainers:
* Johannes De Ridder (johannesdr) - http://drupal.org/user/552788
* tablebooker team - ask@tablebooker.be

Information about tablebooker/tablemanager: http://www.tablebooker.be
